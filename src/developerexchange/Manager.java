package developerexchange;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.StaleProxyException;
import java.util.HashMap;

public class Manager extends Agent
{

    private int workers_per_project;
    private int total_project_count;
    private int finished_project_count;
    private int countOfTypedWorkers = 0;
    private double average_skill = 0.0;
    private boolean allow_exchange = false;
    private StringBuilder result_sb;

    @Override
    protected void setup()
    {
        Object[] args = getArguments();
        if(args.length == 1) {
            workers_per_project = Integer.parseInt(args[0].toString());
        } else {
            workers_per_project = 2;
        }

        result_sb = new StringBuilder();

        addBehaviour(new InitialProjectFinder(this, 100));
        addBehaviour(new ProjectRequester(this, 100));

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("manager");
        sd.setName("Manager");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    @Override
    public void takeDown()
    {
        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    private DFAgentDescription[] findAgentsByType(String type) {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType(type);
        template.addServices(sd);
        try
        {
            return DFService.search(this, template);
        } catch (FIPAException ex) {
            ex.printStackTrace();
            return new DFAgentDescription[]{};
        }
    }

    // Определяет общее число проектов
    public class InitialProjectFinder extends TickerBehaviour
    {
        public InitialProjectFinder(Agent a, long d)
        {
            super(a,d);
        }

        @Override
        public void onTick()
        {
            DFAgentDescription[] desc = findAgentsByType("project");
            if(desc.length > total_project_count)
                total_project_count = desc.length;
        }
    }


    // Выдает типы проектам в зависимости от текущего распределения
    public class LoadBalancer extends CyclicBehaviour
    {
        private MessageTemplate mt;

        public LoadBalancer(Agent a)
        {
            super(a);
            mt = MessageTemplate.and(
                MessageTemplate.MatchConversationId("get-type"),
                MessageTemplate.or(
                    MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                    MessageTemplate.MatchPerformative(ACLMessage.CONFIRM)));
        }

        @Override
        public void action()
        {
            ACLMessage msg = myAgent.receive(mt);
            if(msg == null) {
                block();
                return;
            }

            if(msg.getPerformative() == ACLMessage.CONFIRM)
            {
                countOfTypedWorkers++;
                if((countOfTypedWorkers >= total_project_count) && !allow_exchange) {
                    allow_exchange = true;
                    myAgent.addBehaviour(new AllowExchange(myAgent));
                }

                System.out.println("[MTYPE]\tConfirmed type of " + countOfTypedWorkers + " workers out of " + total_project_count);
                return;
            }


            int countOfInitiators = findAgentsByType("initiator").length;
            int countOfFollowers  = findAgentsByType("simple").length;


            ACLMessage reply = msg.createReply();

            reply.setPerformative(ACLMessage.INFORM);
            if(countOfInitiators > countOfFollowers) {
                reply.setContent("simple;" + average_skill);
            }
            if(countOfInitiators < countOfFollowers) {
                reply.setContent("initiator;" + average_skill);
            }
            if(countOfInitiators == countOfFollowers) {                
                reply.setContent("initiator;" + average_skill);
            }
            myAgent.send(reply);
        }
    }


    public class AllowExchange extends CyclicBehaviour
    {
        AllowExchange(Agent a) {super(a);}

        MessageTemplate mt = MessageTemplate.and(
            MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
            MessageTemplate.MatchConversationId("allow-exchange"));

        @Override 
        public void action() {
            ACLMessage msg = myAgent.receive(mt);
            if(msg == null) {
                block();
                return;
            }
            ACLMessage reply = msg.createReply();
            reply.setPerformative(allow_exchange ? ACLMessage.CONFIRM : ACLMessage.REFUSE);
            myAgent.send(reply);
        }
    }


    public class ProjectRequester extends TickerBehaviour
    {

        HashMap<AID, Integer> projectAgents = new HashMap<>();
        int step = 0;
        int readyprojects = 0;

        public ProjectRequester(Agent a, long period)
        {
            super(a, period);
        }

        @Override
        protected void onTick()
        {
            switch (step)
            {
                case 0: // Определение глобального среднего скилла
                    MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    ACLMessage msg = myAgent.receive(mt);
                    if (msg != null)
                    {
                        Developer[] devs = Developer.parseDevelopers(msg.getContent());
                        double skillavg = 0.0;
                        for(Developer d : devs)
                            skillavg += d.getSkill();

                        skillavg /= (devs.length);

                        System.out.println("[MANGR]\tAverage skill of " + msg.getSender().getLocalName() + " is " + skillavg);

                        AID project = msg.getSender();
                        projectAgents.put(project, (int)skillavg);
                        if (projectAgents.size() == total_project_count && projectAgents.size() != 0)
                        {
                            step = 1;
                            System.err.println("[MANGR]\tAll projects got their initial developers");
                        }
                    } else
                    {
                        block();
                    }
                    
                    break;
                case 1:
                    int summary = 0;
                    for (int c : projectAgents.values())
                    {
                        summary += c;
                    }
                    System.out.print("[MANGR]\tSum skill: " + summary);
                    average_skill = summary / (total_project_count*1.0);
                    System.out.println(", count: " + total_project_count +", average skill: "+ average_skill);
                    
                    step = 2;
                    break;
                case 2: // Начать распределение типов
                    myAgent.addBehaviour(new LoadBalancer(myAgent));
                    step = 3;
                    break;
                case 3: // Ожидание завершения обмена и печать результатов
                    mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    msg = myAgent.receive(mt);
                    if (msg != null)
                    {
                        System.out.println("[MANGR]\t" + msg.getSender().getLocalName() + " exchanged their developers!");


                        result_sb.append("\n\nProject\t");
                        result_sb.append(msg.getSender().getLocalName());
                        result_sb.append("\n    Developers:");

                        Developer[] team = Developer.parseDevelopers(msg.getContent());

                        double skill = 0;
                        for(Developer d : team) {
                            result_sb.append("\n\t");
                            result_sb.append(d.getName());
                            result_sb.append("\t(");
                            result_sb.append(d.getQualification());
                            result_sb.append(")\t skill: ");
                            result_sb.append(d.getSkill());
                            skill += d.getSkill();
                        }

                        result_sb.append("\n    Average skill:\n\t\t");
                        result_sb.append(skill / team.length);

                        finished_project_count++;
                        if (total_project_count <= finished_project_count)
                        {
                            System.out.println();
                            System.out.println("[MANGR]\tAll projects have finished exchange and are ready!");
                            System.out.println();
                            System.out.println(result_sb.toString());
                            
                            step = 5;
                        }
                    } else
                    {
                        block();
                    }
                    break;
                case 5:
                    try {
                        myAgent.doDelete();
                        myAgent.getContainerController().kill();
                    }
                    catch(StaleProxyException e)
                    {
                        e.printStackTrace();
                    }
            }
        }

    }
}
