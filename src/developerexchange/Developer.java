package developerexchange;

import java.lang.StringBuilder;

public class Developer
{
    private String qualification;
    private String name;
    private int skill;
    
    public Developer(String qualification, String name, int skill)
    {
        this.qualification = qualification;
        this.name = name;
        this.skill = skill;
    }
    
    public Developer(String str)
    {
        String[] elems = str.split(";");
        try {
            this.qualification = elems[0];
            this.name = elems[1];
            this.skill = Integer.parseInt(elems[2]);
        } catch (Exception e) {
            System.out.println("Invalid developer: " + str);
            e.printStackTrace();
        }
    }

    public static Developer[] parseDevelopers(String devs) 
    {
        String[] ds = devs.split(":");
        int length = ds.length;
        Developer[] result = new Developer[length];

        for(int i = 0; i < length; ++i) {
            result[i] = new Developer(ds[i]);
        }
        return result;
    }

    public static String toString(Developer[] devs)
    {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < devs.length; ++i) {
            if(devs[i] != null) {
                if(i != 0) sb.append(":");
                sb.append(devs[i].toString());
            }
        }
        return sb.toString();
    }
    
    @Override
    public String toString()
    {
        return qualification + ";" + name + ";" + skill;
    }

    public String getName()
    {
        return name;
    }

    public String getQualification()
    {
        return qualification;
    }

    public int getSkill()
    {
        return skill;
    }

    public boolean isSame(Developer other) {
        return (name == other.name) && (qualification == other.qualification) && (skill == other.skill);
    }
}
