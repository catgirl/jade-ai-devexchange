package developerexchange;

import jade.core.Agent;
import jade.core.Runtime;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.wrapper.StaleProxyException;

public class BeginWork extends Agent {
    
    @Override
    protected void setup() {
        addBehaviour(new OneShotBehaviour(this) {
            @Override
            public void action()
            {
                ACLMessage msg = new ACLMessage(ACLMessage.CFP);

                for(DFAgentDescription d : findAgentsByType("project")) {
                    msg.addReceiver(d.getName());
                }

                msg.setContent("Begin Work!");
                myAgent.send(msg);
                try {
                    myAgent.doDelete();
                    myAgent.getContainerController().kill();
                }
                catch(StaleProxyException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }
    
    private DFAgentDescription[] findAgentsByType(String type) {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType(type);
        template.addServices(sd);
        try
        {
            return DFService.search(this, template);
        } catch (FIPAException ex) {
            ex.printStackTrace();
            return new DFAgentDescription[]{};
        }
    }
        
}
