package developerexchange;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.lang.Math;
import java.time.Instant;
import java.time.temporal.Temporal;
import java.time.temporal.ChronoUnit;

public class ProjectAgent extends Agent
{

    private Developer[] my_developers;
    private double global_average_skill = 0.0;
    private double current_average_skill = 0.0;
    private int worker_count = 0;
    private int exchange_count = 0;
    private int failure_count = 0;
    private boolean is_initiator = false;

    @Override
    protected void setup()
    {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("project");
        sd.setName(getLocalName());
        dfd.addServices(sd);
        try
        {
            DFService.register(this, dfd);
        } catch (FIPAException fe)
        {
            fe.printStackTrace();
        }

        Object[] args = getArguments();
        if(args.length == 1) {
            worker_count = Integer.parseInt(args[0].toString());
        } else {
            System.err.println("[ERR]\tProject\t" + getLocalName() + " did not received worker count!");
            takeDown();
        }

        my_developers = new Developer[worker_count];

        addBehaviour(new AllowDeveloperReqeuesting(this));
        System.out.println("[INIT]\tProject\t" + this.getLocalName() + " is ready to invite " + worker_count + " workers!");
    }

    @Override
    public void takeDown()
    {
        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    // Поиск агента по типу
    private DFAgentDescription[] findAgentsByType(String type) {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType(type);
        template.addServices(sd);
        try
        {
            return DFService.search(this, template);
        } catch (FIPAException ex) {
            ex.printStackTrace();
            return new DFAgentDescription[]{};
        }
    }

    // Сравнение уровня скилла со средним 
    private static boolean checkSkillAverage(double our, double global)
    {
        return (our < (global + global/20.0) && (our > (global - global/20)));
    }

    // Сравнение скилла относительно среднего
    private static boolean checkSkillBetterThan(double newavg, double oldavg, double global)
    {
        return Math.abs(newavg - global) < Math.abs(oldavg - global);
    }

    private void updateAverage()
    {
        double skill = 0.0;
        for(Developer d : my_developers) {
            skill += d.getSkill();
        }
        skill /= worker_count;
        current_average_skill = skill;
    }

    private int findDev(Developer[] devs, Developer need)
    {
        for(int i = 0; i < devs.length; ++i) {
           //System.out.println("comparing " + devs[i] + " and " + need);
            if(devs[i].toString().equals(need.toString()))
                return i;
        }
        return -1;
    }

    private void doExchange(Developer[] exchange_pair)
    {
        if(exchange_pair != null && exchange_pair.length == 2)
        {
            int pos1 = findDev(my_developers, exchange_pair[0]);
            int pos2 = findDev(my_developers, exchange_pair[1]);
            if(pos1 <0 && pos2 < 0)
                return;

            if(pos1 >= 0) {
                System.out.println("\n[SUCCESS]\tProject\t" + getLocalName() + "\t Has exchanged\t" + my_developers[pos1] + "\t->\t" + exchange_pair[1]);
                my_developers[pos1] = exchange_pair[1];
            }
            if(pos2 >= 0) {
                System.out.println("\n[SUCCESS]\tProject\t" + getLocalName() + "\t Has exchanged\t" + my_developers[pos2] + "\t->\t" + exchange_pair[0]);
                my_developers[pos2] = exchange_pair[0];
            }
        }
    }

    // Поиск лучшей пары работников для обмена с точки зрения среднего скилла обоих проектов
    Developer[] findExhangeOption(Developer[] first, Developer[] second) {
        int our_skill_sum = 0;
        int their_skill_sum = 0;
        for(Developer f : first)
            our_skill_sum += f.getSkill();
        for(Developer s : second)
            their_skill_sum += s.getSkill();

        double our_avg = our_skill_sum / (double)first.length;
        double their_avg = their_skill_sum / (double)second.length;

        // Check if both are already good
        if(checkSkillAverage(our_avg,  global_average_skill) &&
           checkSkillAverage(their_avg, global_average_skill)) {
            System.out.println("[DBG]\tAlready good!");
            return new Developer[]{};
        }

        double our_avg_t = our_avg, their_avg_t = their_avg;
        Developer[] res = new Developer[2];

        for(Developer first_dev : first)
        for(Developer second_dev : second) 
        {
            int our_new_skill = our_skill_sum - first_dev.getSkill() + second_dev.getSkill();
            int their_new_skill = their_skill_sum - second_dev.getSkill() + first_dev.getSkill();
            double our_newavg = our_new_skill / (double)first.length;
            double their_newavg = their_new_skill / (double)second.length;

            if(checkSkillAverage(our_newavg,    global_average_skill) &&
                checkSkillAverage(their_newavg, global_average_skill))
            { // Found perfect match
                System.out.print("[DBG]\t\tfound perfect match: ");
                System.out.println("our new average:  " + our_newavg + " (was " + our_avg + ")\ttheir new avg:   " + their_newavg + " (was " + their_avg + ")");
                res[0] = first_dev;
                res[1] = second_dev;
                return res;
            }

            if(checkSkillBetterThan(our_newavg, our_avg_t, global_average_skill) &&
                checkSkillBetterThan(their_newavg, their_avg_t, global_average_skill))
            {
                our_avg_t = our_newavg;
                their_avg_t = their_newavg;
                res[0] = first_dev;
                res[1] = second_dev;
            }
        }

        if(res[0] == null || res[1] == null)
            return new Developer[]{};


        System.out.print("[DBG]\t\t");
            int our_new_skill = our_skill_sum - res[0].getSkill() + res[1].getSkill();
            int their_new_skill = their_skill_sum - res[1].getSkill() + res[0].getSkill();
            double our_newavg = our_new_skill / (double)first.length;
            double their_newavg = their_new_skill / (double)second.length;
        System.out.println("our new average:  " + our_newavg + " (was " + our_avg + ")\ttheir new avg:   " + their_newavg + " (was " + their_avg + ")");

        return res;
    }

    private void initExchange(){
        is_initiator = false;
        try {
            DFAgentDescription dfd = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("project");
            sd.setName("MyProject");
            dfd.setName(getAID());
            dfd.addServices(sd);
            DFService.deregister(this);
            DFService.register(this, dfd);
        } catch(FIPAException fe) {
            fe.printStackTrace();
            is_initiator = false;
            return;
        }

        if(exchange_count > worker_count || failure_count > worker_count*1.35) {
            System.out.println("[END]\tProject\t" + getLocalName() + "\thas finished exchange.");

            ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
            for(DFAgentDescription d : findAgentsByType("manager"))
                msg.addReceiver(d.getName());
            msg.setContent(Developer.toString(my_developers));
            this.send(msg);
            takeDown();
        } else {
            addBehaviour(new TypeRequester(this));
        }
    }

    // Ожидает разрешения на набор работников
    private class AllowDeveloperReqeuesting extends Behaviour
    {

        boolean allowed = false;
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
        
        public AllowDeveloperReqeuesting(Agent a)
        {
            super(a);
        }

        @Override
        public void action() {
            ACLMessage msg = myAgent.receive(mt);
            if(msg == null) {
                block(); return;
            }

            allowed = true;
        }

        @Override
        public boolean done()
        {
            if(allowed) {
                myAgent.addBehaviour(new DeveloperRequester(myAgent, 250));
            }
            return allowed;
        }
    }

    // Начальное назначение работников
    private class DeveloperRequester extends TickerBehaviour
    {
        boolean finished = false;

        public DeveloperRequester(Agent a, long period)
        {
            super(a, period);
        }

        @Override
        public void onTick()
        {
            if (finished) {
                stop();
                return;
            }

            boolean filled = true;
            for(Developer d : my_developers)
                if(d == null) filled = false;

            if (filled) // Сообщаем менеджеру, что мы заполнены
            {

                try {
                    AID manager = findAgentsByType("manager")[0].getName();  
                    ACLMessage message = new ACLMessage(ACLMessage.INFORM);
                    message.addReceiver(manager);
                    message.setContent(Developer.toString(my_developers));
                    message.setReplyWith("ready" + System.currentTimeMillis());
                    myAgent.send(message);
                    finished = true;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {

                DFAgentDescription[] result = findAgentsByType("Developer");
                ACLMessage message = new ACLMessage(ACLMessage.REQUEST);

                for (DFAgentDescription desc : result)
                {
                    message.addReceiver(desc.getName());
                }
                message.setContent("I choose you!");
                message.setConversationId("initial-request");
                message.setReplyWith("request" + System.currentTimeMillis());
                myAgent.send(message);
                myAgent.addBehaviour(new DeveloperPicker());
            }
        }

        // принимает сообщения от разработчиков, которые готовы вступить в проект, и проверяет, можно ли их взять
        private class DeveloperPicker extends Behaviour 
        {
            int step = 0;
            int current_dev = 0;
            ACLMessage msg;
            MessageTemplate mt;

            private void setDeveloper(int number, Developer d)
            {
                my_developers[number] = d;
            }

            @Override
            public void action()
            {
                switch (step) {
                case 0:
                    mt = MessageTemplate.and(
                        MessageTemplate.MatchPerformative(ACLMessage.PROPOSE), 
                        MessageTemplate.MatchConversationId("initial-request"));
                    msg = myAgent.receive(mt);

                    if(msg == null) {
                        block();
                        break;
                    }

                    current_dev = -1;
                    for(int i = 0; i < my_developers.length; ++i)
                        if(my_developers[i] == null) {
                            current_dev = i;
                            break;
                        }

                    if (current_dev >= 0)
                    {
                        ACLMessage reply = msg.createReply();
                        reply.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                        reply.setContent("Taking");
                        myAgent.send(reply);
                        step = 1;
                        break;
                    } 
                    break;
                case 1:
                        mt = MessageTemplate.or(
                            MessageTemplate.MatchPerformative(ACLMessage.AGREE),
                            MessageTemplate.MatchPerformative(ACLMessage.REFUSE));
                        msg = myAgent.receive(mt);

                        if(msg == null) {
                            block();
                            break;
                        }

                        if (msg.getPerformative() == ACLMessage.AGREE) {
                            Developer curdev = new Developer(msg.getContent());
                            setDeveloper(current_dev, curdev);
                            // System.out.println("[PICK]\tProject\t" 
                            //     + myAgent.getLocalName() 
                            //     + " have chosen developer #" + current_dev 
                            //     + " = " + curdev.getName() 
                            //     + " (" + curdev.getQualification() + ")");
                        }

                        if (msg.getPerformative() == ACLMessage.REFUSE)
                        {
                            setDeveloper(current_dev, null);
                        }

                        step = 0;
                        break;
                }
            }

            @Override
            public boolean done()
            {
                for(Developer d : my_developers)
                    if(d == null)
                        return false;


                System.out.println("[PICK]\tProject\t"+myAgent.getLocalName() + " finished picking developers...");
                initExchange();
                return true;
            }
        }
    }

    // Получает наш тип и средний скилл у менеджера
    private class TypeRequester extends Behaviour
    {
        boolean haveSent = false;
        String type;
        MessageTemplate mt = MessageTemplate.and(
            MessageTemplate.MatchConversationId("get-type"),
            MessageTemplate.MatchPerformative(ACLMessage.INFORM));

        TypeRequester(Agent a) 
        {
            super(a);
        }

        @Override
        public void onStart()
        {
            is_initiator = false;
        }

        @Override
        public void action()
        {
            if(!haveSent) {
                ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                msg.setConversationId("get-type");
                msg.setContent("I want to know my type!");

                DFAgentDescription mangrs[] = findAgentsByType("manager");
                if(mangrs.length == 0) {
                    System.err.println("Could not find manager!");
                    return;
                }

                for(DFAgentDescription mgr : mangrs)
                    msg.addReceiver(mgr.getName());

                myAgent.send(msg);
                haveSent = true;
                //System.out.println("[TYPE]\tSent type request to manager...");
            } else {
                ACLMessage msg = myAgent.receive(mt);
                if(msg == null) {
                    block();
                    return;
                }

                if(msg.getContent() == null) {
                    System.err.println("[TYPE]\tInvalid type message!");
                    haveSent = false;
                    return;
                }

                try {
                    String[] rs = msg.getContent().split(";");
                    type = rs[0];
                    global_average_skill = Double.parseDouble(rs[1]);
                } catch (Exception e) {
                    System.err.println("Couldn't parse type message " + msg.getContent());
                    haveSent = false;
                    return;
                }

                ACLMessage reply = msg.createReply();
                reply.setPerformative(ACLMessage.CONFIRM);
                reply.setContent("I changed type");
                myAgent.send(reply);
                //System.out.println("[TYPE]\tSent type confirmation to manager!");
            }
        }

        @Override
        public boolean done()
        {
            if(!is_initiator && type !=null) {
                if(type.equals("initiator")) {
                    is_initiator = true;
                }

                try {
                    DFAgentDescription dfd = new DFAgentDescription();
                    ServiceDescription sd = new ServiceDescription();
                    sd.setType(type);
                    sd.setName("MyProject");
                    dfd.setName(getAID());
                    dfd.addServices(sd);
                    DFService.deregister(this.myAgent);
                    DFService.register(this.myAgent, dfd);
                } catch(FIPAException fe) {
                    fe.printStackTrace();
                    is_initiator = false;
                    myAgent.doDelete();
                    return false;
                }

                myAgent.addBehaviour(new AllowExchange(myAgent));
                System.out.println("[TYPE]\tProject\t" + getLocalName() + " received type " + type + " and average skill of: " + global_average_skill);
                return true;
            }

            return false;

        }
    }

    // Спрашивает менеджера, разрешен ли обмен
    private class AllowExchange extends Behaviour
    {
        private int step = 0;

        AllowExchange(Agent a)
        {
            super(a);
        }

        @Override
        public void action()
        {
            switch(step) {
            case 0:
                ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                msg.setConversationId("allow-exchange");
                for(DFAgentDescription df : findAgentsByType("manager"))
                    msg.addReceiver(df.getName());
                myAgent.send(msg);
                step = 1;
                break;

            case 1: // Ожидаем сигнала о готовности к обмену у менеджера
                MessageTemplate mt = MessageTemplate.MatchConversationId("allow-exchange");
                msg = myAgent.receive(mt);
                if(msg == null) {
                    block();
                    break;
                }

                if (msg.getPerformative() == ACLMessage.CONFIRM) {
                    step = 10;
                } else {
                    System.err.println("[WARN]\tUnexpected reply from manager: " + msg.getContent() + ", expected 'Begin exchange!'");
                }
                break;
            }
        }

        @Override
        public boolean done()
        {
            if(step > 1) {
                if(is_initiator)
                    myAgent.addBehaviour(new InitiatorExchanger(myAgent, 100));
                else myAgent.addBehaviour(new ReceiverExchanger(myAgent, 100));
                return true;
            }
            return false;
        }


    }

    // Реализует обмен инициатором
    private class InitiatorExchanger extends TickerBehaviour
    {
        private int step = 0;
        private Developer[] exchange_pair;
        private Instant start_time;

        public InitiatorExchanger(Agent a, long p)
        {
            super(a,p);
            start_time = Instant.now();
        }

        private void dieAndRestart()
        {
            stop();
            initExchange();
        }

        private void checkTimeout()
        {
            long millisecs = ChronoUnit.MILLIS.between(start_time, Instant.now());
                if(millisecs > 5000) {
                System.out.println("[TIMEOUT]\tProject\t" + getLocalName());
                dieAndRestart();
            }
        }

        @Override
        public void onTick() { checkTimeout(); switch(step) {
        case 0: // Отправляем всем simple запрос на обмен

            DFAgentDescription[] result = findAgentsByType("simple");
            if(result.length == 0) {
                System.err.println("[IXCHG FAILURE]\tNo simples found.");
                ++failure_count;
                dieAndRestart();
                return;
            }

            ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
            for (DFAgentDescription project : result)
            {
                 msg.addReceiver(project.getName());
            }

            msg.setContent("Do you want to exchange developers?");
            msg.setConversationId("developer-exchange-begin");
            msg.setReplyWith("request" + System.currentTimeMillis());
            myAgent.send(msg);
            ++step;
            System.out.println();

            break;
        case 1: // Ожидаем ответа от simple-ов с его работниками

            msg = myAgent.receive(MessageTemplate.MatchConversationId("developer-exchange-begin"));
            if(msg == null) { block(); return; }

            // Интересуют только предложения
            if(msg.getPerformative() != ACLMessage.PROPOSE)
                return;

            // Парсим их работников
            Developer[] their_devs = Developer.parseDevelopers(msg.getContent());

            if(their_devs.length != worker_count) {
                System.err.println("Bad response from " + msg.getSender().getLocalName() + ":\t" + msg.getContent());
                return;
            }

            // Определяем пару для обмена
            Developer[] exchange_option = findExhangeOption(my_developers, their_devs);

            // если обмен нужен
            if (exchange_option.length == 2) { 
                ACLMessage reply = msg.createReply();
                reply.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                reply.setContent(Developer.toString(exchange_option)); 
                myAgent.send(reply);
                ++step;
                System.out.println("[ACCEPT_PROPOSAL]\t" + getLocalName() + " (" + exchange_option[0].getSkill() + ")\twith\t" + msg.getSender().getLocalName() + "(" + exchange_option[1].getSkill() + ")");
            } else {
                System.out.println("[NO NEED]\t" + getLocalName() + "\twith\t" +  msg.getSender().getLocalName());
                ++failure_count;
                return; // обмен c этим проектом не нужен
            }
            break;

        case 2: // Получаем согласие на обмен и начинаем его

            msg = myAgent.receive(MessageTemplate.MatchConversationId("developer-exchange-perform"));
            if(msg == null) {block(); return; }

            // интересует только согласие
            if(msg.getPerformative() != ACLMessage.AGREE) {
                step = 1;
                return;
            }

            Developer[] parsed_devs = Developer.parseDevelopers(msg.getContent());

            int pos1 = findDev(my_developers, parsed_devs[0]);
            int pos2 = findDev(my_developers, parsed_devs[1]);

            if(pos1 < 0 && pos2 < 0) {

                System.err.println("[INXCHG FAILURE]\t" + getLocalName() + "\tCould not find our developer in exchange agreement from " + msg.getSender().getLocalName());
                ACLMessage reply = msg.createReply();
                reply.setPerformative(ACLMessage.REFUSE);
                myAgent.send(reply);
                ++failure_count;
                dieAndRestart();
                return;
            }

            // Запоминаем, кого нужно обменять
            exchange_pair = parsed_devs;

            ACLMessage reply  = msg.createReply();
            reply.setPerformative(ACLMessage.CONFIRM);
            reply.setContent(msg.getContent());
            myAgent.send(reply);
            ++step;

            break;
        case 3: // Совершаем обмен
            msg = myAgent.receive(MessageTemplate.MatchConversationId("developer-exchange-finish"));
            if(msg == null) { block(); return; }

            if(msg.getPerformative() != ACLMessage.CONFIRM) {
                System.out.println("[ERR]\tDid not receive exchange finish confirmation");
                ++failure_count;
                dieAndRestart();
                return;
            }

            doExchange(exchange_pair);
            updateAverage();
            ++step;


            break;
        case 4: // Финал обмена
            System.out.println("\n[INITIATOR SUCCESS]\n");
            ++exchange_count;
            dieAndRestart();
            break;

        default:
            System.out.println("[FATAL]\tHow did i get here??");
            dieAndRestart();

        }}
    }


    private class ReceiverExchanger extends TickerBehaviour
    {
        private int step = 0;
        private Developer[] exchange_pair;
        private Instant start_time;

        public ReceiverExchanger(Agent a, long t)
        {
            super(a,t);
            start_time = Instant.now();
        }

        private void dieAndRestart()
        {
            stop();
            initExchange();
        }

        private void checkTimeout()
        {
            long millisecs = ChronoUnit.MILLIS.between(start_time, Instant.now());
            if(millisecs > 5000) {
                System.out.println("[TIMEOUT]\tProject\t" + getLocalName());
                dieAndRestart();
            }
        }

        @Override
        public void onTick() { checkTimeout(); switch(step)
        {
        case 0:
            ACLMessage msg = myAgent.receive(MessageTemplate.MatchConversationId("developer-exchange-begin"));
            if(msg == null) {block(); return;}

            // Предлагаем своих работников в ответ
            if(msg.getPerformative() == ACLMessage.REQUEST) {
                    ACLMessage reply = msg.createReply();
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setContent(Developer.toString(my_developers));
                    myAgent.send(reply);
            }

            // Принимаем предложение об обмене
            if(msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {
                Developer[] proposed_devs = Developer.parseDevelopers(msg.getContent());

                if(proposed_devs.length != 2) {
                    System.err.println("Invalid developer proposal: " + msg.getContent());
                    return;
                }

                int pos1 = findDev(my_developers, proposed_devs[0]);
                int pos2 = findDev(my_developers, proposed_devs[1]);

                if(pos1 < 0 && pos2 < 0) {
                    System.err.println("[SIMPLE XCHG FAILURE]\t" + getLocalName() + " <- " + msg.getSender().getLocalName() + "\tPROPOSAL: " + proposed_devs[0] + " for " + proposed_devs[1]);
                    System.out.println("\tOur devs:\t" + my_developers[0] + "\t" + my_developers[1] + "\t" + my_developers[2]);
                    ACLMessage reply = msg.createReply();
                    reply.setConversationId("developer-exchange-perform");
                    reply.setPerformative(ACLMessage.REFUSE);
                    myAgent.send(reply);
                    ++failure_count;
                    return;
                }

                // Запоминаем, кого нужно обменять
                exchange_pair = proposed_devs;
                ++step;

                ACLMessage reply = msg.createReply();
                reply.setContent(msg.getContent());
                reply.setConversationId("developer-exchange-perform");
                reply.setPerformative(ACLMessage.AGREE);
                myAgent.send(reply);
            }
            break;

        case 1: // Ожидаем подверждения обмена
            msg = myAgent.receive(MessageTemplate.MatchConversationId("developer-exchange-perform"));
            if(msg == null) {block(); return;}

            if(msg.getPerformative() != ACLMessage.CONFIRM) {
                ++failure_count;
                dieAndRestart();
                return;
            }

            doExchange(exchange_pair);
            updateAverage();
            ++step;

            ACLMessage reply = msg.createReply();
            reply.setPerformative(ACLMessage.CONFIRM);
            reply.setConversationId("developer-exchange-finish");
            myAgent.send(reply);
            break;

        case 2:
            System.out.println("\n[SIMPLE SUCCESS]\n");
            ++exchange_count;
            dieAndRestart();
            break;

        default:
            System.out.println("[FATAL]\tHow did I got here?");
            dieAndRestart();
        }}
    }
        
}


