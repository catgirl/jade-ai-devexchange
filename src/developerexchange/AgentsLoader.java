package developerexchange;

import jade.core.Agent;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class AgentsLoader extends Agent {

    private int worker_count = 0;

    @Override
    protected void setup() {
        Object[] args = getArguments();
        if(args.length != 1) {
            System.err.println("Please specify input file!");
            return;
        }

        BufferedReader reader = null;
        int lineCount = 0;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(args[0].toString()), "utf-8"));
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                lineCount++;
                AgentController ac = parseAgent(currentLine);
                if (ac != null) {
                    ac.start();
                }
            }
        } catch (IOException ex) {
            System.out.println("Reading error in line " + lineCount);
        } catch (StaleProxyException ex) {
            ex.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                System.out.println("Can't close the file");
            }
        }
    }
    
    private AgentController parseAgent(String s) throws StaleProxyException {
        String[] splitted = s.split(";");
        
        switch (splitted[0]) {
            case "developer":               
                String agentName = splitted[2].toLowerCase() + "_" + splitted[1];
                
                String qualification = splitted[2];
                String name = splitted[3];
                int skill = Integer.parseInt(splitted[4]);

                Object[] args = new Object[] {qualification, name, skill};
                
                return getContainerController().createNewAgent(agentName, "developerexchange.DeveloperAgent", args);

            case "project":
                String projectName = splitted[1];
                return getContainerController().createNewAgent("project_"+projectName, "developerexchange.ProjectAgent", new Object[]{worker_count});
            case "manager":
                String managerName = splitted[1];
                int devcount = 2;
                if(splitted.length == 3) {
                    devcount = Integer.parseInt(splitted[2]);
                    worker_count = devcount;
                }
                return getContainerController().createNewAgent("manager_"+managerName, "developerexchange.Manager", new Object[] {devcount});
                
            default:
                return null;
        }        
    }
}
