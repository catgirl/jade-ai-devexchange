package developerexchange;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;



public class DeveloperAgent extends Agent {
    
    Developer d;

    @Override
    protected void setup() {
        Object[] args = getArguments();

        if (args != null && args.length == 3) {
            
            String qualification = args[0].toString();
            String name = args[1].toString();       
            
            try {
                int skill = Integer.parseInt(args[2].toString());
                d = new Developer(qualification, name, skill);
            } catch (NumberFormatException ex) {
                System.out.println("Can't parse developer's skill...");
                this.takeDown();
            }
        } 
        else {
            /* Удаляем агента если не заданы параметры */
            this.takeDown();         
            return;
        }
        
        /* Регистрируем агента в системе */      
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Developer");
        sd.setName(d.getName());
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        addBehaviour(new CyclicBehaviour() {
            boolean isBusy = false;

            @Override
            public void action()
            {
                ACLMessage msg = myAgent.receive();
                if(msg == null) {
                    block();
                    return;
                }

                // Отвечаем на первый запрос, предлагаем себя
                if (msg.getPerformative() == ACLMessage.REQUEST && !isBusy)
                {
                    ACLMessage reply = msg.createReply();
                    reply.setContent(d.toString());
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setConversationId("initial-request");
                    myAgent.send(reply);
                    return;
                }

                // Если нас выбрали
                if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL)
                {
                    if(isBusy) { // Если мы уже заняты, отказываем
                        ACLMessage reply = msg.createReply();
                        reply.setContent(d.toString());
                        reply.setPerformative(ACLMessage.REFUSE);
                        myAgent.send(reply);
                        //System.out.println("[PICK]\t" + d.getName() + " refused to join " + msg.getSender().getLocalName() + " because he's already taken");
                    } else { // Соглашаемся
                        isBusy = true;
                        ACLMessage reply = msg.createReply();
                        reply.setContent(d.toString());
                        reply.setPerformative(ACLMessage.AGREE);
                        myAgent.send(reply);
                        //System.out.println("[PICK]\t" + d.getName() + " agreed to join " + msg.getSender().getLocalName());
                    }
                }
            }
        });
        System.out.println("[INIT]\tWorker\t" + d.getName() + " is ready!");
    }

    @Override
    public void takeDown()
    {
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }
}
