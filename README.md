# README #

This is an agent-based AI system I wrote for university assignment. Based on the [work of my classmates](https://github.com/LinarAbzaltdinov/ExamCardsCreator).

The program reads a list of developers and projects and creates corresponding agents. 
Then the agents begin communicating with each other in order to assign the required number of developers to each project team, while taking individual skill level of developers into account, so that the average skill levels of all teams are close to each other.